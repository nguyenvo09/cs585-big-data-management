package problem2;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
/**
 * 
 * @author ben
 *
 */
public class JSONRecordReader extends RecordReader<LongWritable, Text> {
	private byte[] startTag;
	private byte[] endTag;
	private long start;
	private long end;
	private FSDataInputStream fsin;
	private LongWritable key;
	private Text value;
	private final DataOutputBuffer buffer = new DataOutputBuffer();

	@Override
	public LongWritable getCurrentKey() throws IOException,
			InterruptedException {
		// TODO Auto-generated method stub
		return key;
	}

	@Override
	public Text getCurrentValue() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		return ((fsin.getPos() - start) / (float) (end - start));
	}

	@Override
	public void initialize(InputSplit genericSplit, TaskAttemptContext context)
			throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		FileSplit split = (FileSplit) genericSplit;
		Configuration job = context.getConfiguration();

		startTag = "{".getBytes("utf-8");
		endTag = "}".getBytes("utf-8");

		// open the file and seek to the start of the split
		start = split.getStart();
		end = start + split.getLength();
		Path file = split.getPath();
		FileSystem fs = file.getFileSystem(context.getConfiguration());
		fsin = fs.open(split.getPath());
		fsin.seek(start);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		if (key == null) {
			key = new LongWritable();
		}
		// key.set(pos);
		if (value == null) {
			value = new Text();
		}
		if (fsin.getPos() < end) {
			if (readUntilMatch(startTag, false)) {
				try {
					buffer.write(startTag);
					if (readUntilMatch(endTag, true)) {
						key.set(fsin.getPos());
						value.set(buffer.getData(), 0, buffer.getLength());
						String s = value.toString();
						String[] args = s.split("\\s+");
						StringBuffer bff = new StringBuffer();
						for (int ii = 1; ii < args.length - 1; ii += 1) {
							bff.append(args[ii]);
						}
						value.clear();
						value.set(bff.toString());
						return true;
					}
				} finally {
					buffer.reset();
				}
			}
		}
		return false;
	}

	private boolean readUntilMatch(byte[] match, boolean withinBlock)
			throws IOException {
		int i = 0;
		while (true) {
			int b = fsin.read();
			// end of file:
			if (b == -1)
				return false;
			// save to buffer:
			if (withinBlock)
				buffer.write(b);

			// check if we're matching:
			if (b == match[i]) {
				i++;
				if (i >= match.length)
					return true;
			} else
				i = 0;
			// see if we've passed the stop point:
			if (!withinBlock && i == 0 && fsin.getPos() >= end)
				return false;
		}
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		if (fsin != null) {
			fsin.close();
		}
	}

}
