package problem3;

/**
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//package org.apache.hadoop.examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import junit.framework.Assert;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * 
 * @author ben
 *
 */
public class Problem3_part4_5a {

	public static String K_INIT_CENTROIDS_FILE = "K_INIT_CENTROIDS_FILE";
	public static String EPSILON_STRING = "EPSILON_STRING";
	public static String CONVERGED = "converged";

	static class Point {
		double x;
		double y;
		String id;
		int cnt = 0;

		public Point(String _id, double _x, double _y) {
			// TODO Auto-generated constructor stub
			this.x = _x;
			this.y = _y;
			this.id = _id;
		}

		public void aggregatePoint(double _x, double _y) {
			this.x += _x;
			this.y += _y;
			cnt += 1;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return String.format("%s,%5f,%5f,%s", id, x, y, cnt);
		}
	}

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, Text> {

		// private final static IntWritable one = new IntWritable(1);
		// private Text word = new Text();
		String k_centroid_init_file = "";
		ArrayList<Point> ls = new ArrayList<Point>();
		HashMap<String, Point> m_centroids;

		private ArrayList<Point> readCentroids(String filename,
				Configuration conf) {
			ArrayList<Point> res = new ArrayList<Problem3_part4_5a.Point>();
			try {
				Path pt = new Path(filename);
				FileSystem fs = FileSystem.get(conf);
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
				int cnt = 0;
				String line = "";
				while ((line = br.readLine()) != null) {
					String[] args = line.split(",");
					String id = args[0];
					double x = Double.parseDouble(args[1]);
					double y = Double.parseDouble(args[2]);
					Point p = new Point(id, x, y);
					res.add(p);
					cnt += 1;
				}
				Assert.assertTrue(cnt >= 1);
				return res;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {

			}
			return res;
		}

		private double Distance(double x1, double y1, double x2, double y2) {
			return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
		}
		
		private void init_map_centroids(){
			for(Point p: ls){
				m_centroids.put(p.id, new Point(p.id, 0, 0));
			}
		}
		
		@Override
		protected void setup(Mapper<Object, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			k_centroid_init_file = context.getConfiguration().get(K_INIT_CENTROIDS_FILE);
			Assert.assertTrue(k_centroid_init_file != null && !k_centroid_init_file.equalsIgnoreCase(""));
			ls = readCentroids(k_centroid_init_file, context.getConfiguration());
			m_centroids = new HashMap<String, Problem3_part4_5a.Point>();
			init_map_centroids();
		}

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			double px = Double.parseDouble(s[0]);
			double py = Double.parseDouble(s[1]);

			double min_dis = -1;
			int min_index = -1;
			for (int i = 0; i < ls.size(); i += 1) {
				Point p = ls.get(i);
				double d = Distance(px, py, p.x, p.y);
				if (min_index == -1) {
					min_dis = d;
					min_index = i;
				} else {
					if (d < min_dis) {
						min_index = i;
						min_dis = d;
					}
				}
			}
			Assert.assertTrue(min_index != -1 && min_dis >= 0);
			// /aggregating centroids
			Point closet_centroid = ls.get(min_index);
			
			Point p = m_centroids.get(closet_centroid.id);
			p.aggregatePoint(px, py);
			m_centroids.put(closet_centroid.id, p);
			
			
		}

		@Override
		protected void cleanup(Context context) throws IOException,
				InterruptedException {
			// TODO Auto-generated method stub
			super.cleanup(context);
			Text k = new Text();
			Text v = new Text();
			for (Entry<String, Point> entry : m_centroids.entrySet()) {
				k.set(entry.getKey());
				v.set(entry.getValue().toString());
				context.write(k, v);
			}

		}
	}

	public static class IntSumCombiner extends Reducer<Text, Text, Text, Text> {

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {

			String cluster_id = key.toString();
			double sumX = 0;
			double sumY = 0;
			int cntPoint = 0;

			for (Text val : values) {
				String s = val.toString();
				String[] args = s.split(",");
				Assert.assertTrue(args[0].equalsIgnoreCase(cluster_id));
				double x = Double.parseDouble(args[1]);
				double y = Double.parseDouble(args[2]);
				double no_points = Integer.parseInt(args[3]);
				sumX += x;
				sumY += y;
				cntPoint += no_points;
			}

			Point p = new Point(cluster_id, sumX, sumY);
			p.cnt = cntPoint; // very important here!!!
			context.write(key, new Text(p.toString()));
		}
	}

	public static class IntSumReducer extends
			Reducer<Text, Text, Text, NullWritable> {
		HashMap<String, Point> m_old_centroids;
		float epsilon = 0.0f;
		/*Read centroids*/
		private HashMap<String, Point> readCentroids(String filename, Configuration conf) {
			HashMap<String, Point> res = new HashMap<String, Point>();
			try {
				Path pt = new Path(filename);
				FileSystem fs = FileSystem.get(conf);
				BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
				int cnt = 0;
				String line = "";
				while ((line = br.readLine()) != null) {
					String[] args = line.split(",");
					String id = args[0];
					double x = Double.parseDouble(args[1]);
					double y = Double.parseDouble(args[2]);
					Point p = new Point(id, x, y);
					res.put(id, p);
					cnt += 1;
				}
				Assert.assertTrue(cnt >= 1);
				return res;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {

			}
			return res;
		}
		
		@Override
		protected void setup(
				Reducer<Text, Text, Text, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			m_old_centroids = readCentroids(context.getConfiguration().get(K_INIT_CENTROIDS_FILE), context.getConfiguration());
			epsilon = context.getConfiguration().getFloat(EPSILON_STRING, 0.5f);
		}
		
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {

			String cluster_id = key.toString();
			double sumX = 0;
			double sumY = 0;
			int cntPoint = 0;

			for (Text val : values) {
				String s = val.toString();
				String[] args = s.split(",");
				Assert.assertTrue(args[0].equalsIgnoreCase(cluster_id));
				double x = Double.parseDouble(args[1]);
				double y = Double.parseDouble(args[2]);
				double no_points = Integer.parseInt(args[3]);
				sumX += x;
				sumY += y;
				cntPoint += no_points;
			}
			double newX = sumX / cntPoint;
			double newY = sumY / cntPoint;
			
			Point old_cluster = m_old_centroids.get(cluster_id);
			boolean thisClusterConverged = false;
			if((Math.abs(old_cluster.x - newX)<=epsilon && Math.abs(old_cluster.y - newY) <=epsilon)){
				thisClusterConverged = true;
			}
			if(thisClusterConverged){
				Text t = new Text(String.format("%s,%.5f,%.5f,converged", cluster_id, newX, newY));
				context.write(t, NullWritable.get());
			}else{
				Text t = new Text(String.format("%s,%.5f,%.5f,nonConverged", cluster_id, newX, newY));
				context.write(t, NullWritable.get());
			}	
		}
		
	}

	public static String generateKInitPoints(int K, Configuration conf,
			String filename) throws IOException {
		PrintWriter writer = new PrintWriter(filename);
		Random r = new Random();
		r.setSeed(1000);
		for (int i = 0; i < K; i += 1) {
			float x = r.nextFloat() * (10000 - 1) + 1;
			float y = r.nextFloat() * (10000 - 1) + 1;
			String s = String.format("c%s,%.2f,%.2f", i, x, y);
			writer.println(s);

		}
		writer.close();
		String hdfs_file = "hdfs_" + filename;
		FileSystem fs = FileSystem.get(conf);
		FileSystem localFileSystem = FileSystem.getLocal(conf);
		FileUtil.copy(localFileSystem, new Path(filename), fs, new Path(hdfs_file), false, conf);
		return hdfs_file;
	}
	
	public static void mergeFiles(String root_folder, String merged_file, FileSystem from, FileSystem to, Configuration conf) {
		try {
			to.delete(new Path(merged_file), false);
			FileUtil.copyMerge(from, new Path(root_folder), to, new Path(merged_file), false, conf, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean checkConverged(String hdfs_out_folder, Configuration conf) throws IOException{
		//we should merge all output file in that folder first.
		FileSystem fs = FileSystem.get(conf);
		FileSystem localFileSystem = FileSystem.getLocal(conf);
		String local_file_new_centroids = "new_centroids_kmean.txt";
		mergeFiles(hdfs_out_folder, local_file_new_centroids, fs, localFileSystem, conf);
		boolean converged = readCentroids(local_file_new_centroids);
		return converged;
	}
	
	private static boolean readCentroids(String filename) throws NumberFormatException, IOException{
		BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
		HashMap<String, Point> map = new HashMap<String, Point>();
		String line = "";
		int cntLine = 0;
		int cntConveged = 0;
		while((line = reader.readLine()) != null){
			String[] args = line.split(",");
			if(args.length == 4){
				if(args[3].equalsIgnoreCase("converged")){
					cntConveged += 1;
				}
			}
			cntLine+=1;
			String id = args[0];
			double x = Double.parseDouble(args[1]);
			double y = Double.parseDouble(args[2]);
			Point p = new Point(id, x, y);
			map.put(id, p);
		}
		if(cntLine == cntConveged){
			return true;
		}
		return false;
	}
	public static void main(String[] args) throws IOException,
			ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
				
		Configuration conf = new Configuration();
		if (args.length != 5) {
			System.err.println("Usage: prob3 <data_points_file> <K> <R> <epsilon> <HDFS output file>");
			System.exit(2);
		}
		int K = Integer.parseInt(args[1]);
		conf.setInt("K", K);

		String k_init_file = "K_init_centers.txt";
		String hdfs_file_centroid = generateKInitPoints(K, conf, k_init_file);
		
		int R = Integer.parseInt(args[2]);
		float epsilon = Float.parseFloat(args[3]);
		conf.setFloat(EPSILON_STRING, epsilon);
//		if(true){
//			return;
//		}
		String outputfolder = args[4];
		
		for (int i = 0; i < R; i += 1) {
			conf.set(K_INIT_CENTROIDS_FILE, hdfs_file_centroid);
			
			Job job = new Job(conf, "prob3_iter" + (i + 1));
			job.setJarByClass(Problem3_part4_5a.class);
			job.setMapperClass(TokenizerMapper.class);
			job.setCombinerClass(IntSumCombiner.class);
			job.setReducerClass(IntSumReducer.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(NullWritable.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(Text.class);
			job.setNumReduceTasks(1);
			FileInputFormat.addInputPath(job, new Path(args[0]));
			FileSystem fs = FileSystem.get(job.getConfiguration());

			fs.delete(new Path(outputfolder), true);

			FileOutputFormat.setOutputPath(job, new Path(outputfolder));
			job.waitForCompletion(true);
			
			//We only need to replace input with new one.
			String local_merged_file_new_centroid = "new_centroid_merged_local.txt";
			mergeFiles(outputfolder, local_merged_file_new_centroid, fs, FileSystem.getLocal(conf), conf);
			
			//upload it back to hdfs
			String hdfs_merged_file_new_centroid = "hdfs_new_centroid_merged_local.txt";
			FileUtil.copy(FileSystem.getLocal(conf), new Path(local_merged_file_new_centroid), fs, new Path(hdfs_merged_file_new_centroid), true, conf);
			
			if(checkConverged(outputfolder, conf)){
				return;
			}
			
			//update new file
			if(i != (R-1)){
				fs.delete(new Path(outputfolder), true);
			}
			
			//update new centroid file
			hdfs_file_centroid = hdfs_merged_file_new_centroid;
			//update folder
			outputfolder = "prob3_out_" + (i+2);
			
			
			
		}

	}

}
