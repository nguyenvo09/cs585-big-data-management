package problem1;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Problem1 {
	public static String windowKey = "windowKey";
	// input: accesslog; <(ID,page), "access">
	public static class MapperForP extends Mapper<Object, Text, Text, Text> {
		
		boolean isQuery2 = false;
		double x1,y1,x2,y2;
		@Override
		protected void setup(Mapper<Object, Text, Text, Text>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
//			super.setup(context);
			String win = context.getConfiguration().get(windowKey);
			if(win.equalsIgnoreCase("none") == false){
				isQuery2 = true;
				String s = win;
				String[] args = s.split(",");
				x1 = Double.parseDouble(args[0]);
				y1 = Double.parseDouble(args[1]);
				x2 = Double.parseDouble(args[2]);
				y2 = Double.parseDouble(args[3]);
			}
		}
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
				//just simply emit points as keys.
				String[] args = value.toString().split(",");
				int x = Integer.parseInt(args[0]);
				int y = Integer.parseInt(args[1]);
				if(isQuery2){
					boolean isInside = false;
					
					int xx1 = (int) x1;
					int yy1 = (int) y1;
					
					if(x1 > xx1){
						//avoid case x1 is 1.0000
						xx1 += 1;
					}
					if(y1 > yy1){
						//avoid case y1 is 1.0000
						yy1 += 1;
					}
					int xx2 = (int)x2;
					int yy2 = (int)y2;
					
					if(x >= xx1 && x <= xx2){
						if(y>=yy1 && y<= yy2){
							isInside = true;
						}
					}
					if(isInside){
						context.write(value, new Text("1"));
					}
				}else{
					context.write(value, new Text("1"));
				}
			
				
		}
	}

	// input: friends; <(ID,friend), "friend">
	public static class MapperForR extends Mapper<Object, Text, Text, Text> {
		
		
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
				Text kk = new Text();
				Text vl = new Text();
				String rect = value.toString();
				String[] args = rect.split(",");
				String id = args[0];
				vl.set(id);
				double x1 = Double.parseDouble(args[1]);
				double y1 = Double.parseDouble(args[2]);
				double x2 = Double.parseDouble(args[3]);
				double y2 = Double.parseDouble(args[4]);
				
				int xx1 = (int) x1;
				int yy1 = (int) y1;
				
				if(x1 > xx1){
					//avoid case x1 is 1.0000
					xx1 += 1;
				}
				if(y1 > yy1){
					//avoid case y1 is 1.0000
					yy1 += 1;
				}
				
				
				int xx2 = (int)x2;
				int yy2 = (int)y2;
				 
				for(int x = xx1; x<=xx2; x+=1){
					for(int y = yy1; y<=yy2; y+=1){
						String s = x + "," + y;
						kk.set(s);
						//For every points that are inside or on the border, 
						//we emit key is that point, vl is rectangle identifer.
						context.write(kk, vl);
					}
				}
		}
	}

	public static class Prob1Reducer extends Reducer<Text, Text, Text, NullWritable> {
		private Text id = new Text();
		private Text friend = new Text();

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			//Note that, there will be likelihood that there are duplicated points. 
			//Therefore, we may have a list of multiple "1" and a list of rectangles.
			Text kk = new Text();
			boolean isARealPoint = false;
			ArrayList<String> vls = new ArrayList<String>();
			for (Text val : values) {
				if(val.toString().equalsIgnoreCase("1")){
					isARealPoint = true;
					continue;
				}
				//writing points.
				String s = "<" + val.toString()+"," + " (" + key.toString() + ")>";
				vls.add(s);
			}
			if(isARealPoint){
				for(String s: vls){
					kk.set(s);
					context.write(kk, NullWritable.get());
				}
			}
		}
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		// TODO Auto-generated method stub
		Configuration conf = new Configuration();
		if (args.length != 3 && args.length != 7) {
			System.err.println("Usage: prob1 <data_Points> <data_rectangles> <HDFS output file>");
			System.err.println("Usage: prob1 <data_Points> <data_rectangles> <x1> <y1> <x2> <y2> <HDFS output file>");
			System.exit(2);
		}
		String output_folder = "";
		if(args.length == 7){
			String s = String.format("%s,%s,%s,%s", args[2], args[3], args[4], args[5]);
			output_folder = args[6];
			conf.set(windowKey, s);
		}else{
			conf.set(windowKey, "none");
			output_folder = args[2];
		}
		
		Job job = new Job(conf, "prob1");
		job.setJarByClass(Problem1.class);
		MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MapperForP.class);
		MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MapperForR.class);
		job.setReducerClass(Prob1Reducer.class);
		
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(NullWritable.class);
		job.setNumReduceTasks(10);
		FileSystem fs = FileSystem.get(job.getConfiguration());

		
		fs.delete(new Path(output_folder), true);
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}

}
