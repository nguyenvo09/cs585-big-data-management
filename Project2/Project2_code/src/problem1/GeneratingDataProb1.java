package problem1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;


/**
 * 
 * @author ben
 *
 */
public class GeneratingDataProb1 {
	
	public void generateDataSet_P() throws FileNotFoundException{
		Random r = new Random();
		int no_points = 11000000;
		PrintWriter writer = new PrintWriter(new File("DataP_large.txt"));
		HashMap<String, Integer> map_points = new HashMap<String, Integer>();
		int left = 1;
		int right = 10000;
		for(int i=0; i<no_points; i+=1){
			int x = r.nextInt(right) + left;
			int y = r.nextInt(right) + left;
			String s = x + "," + y;
			writer.println(s);
			
			
		}
		
		writer.close();
	}
	public void generateDataSet_P_small() throws FileNotFoundException{
		Random r = new Random();
		int no_points = 10000;
		PrintWriter writer = new PrintWriter(new File("DataP_small.txt"));
		HashMap<String, Integer> map_points = new HashMap<String, Integer>();
		int left = 1;
		int right = 10000;
		for(int i=0; i<no_points; i+=1){
			int x = r.nextInt(right) + left;
			int y = r.nextInt(right) + left;
			String s = x + "," + y;
			writer.println(s);
			
			
		}
		
		writer.close();
	}
	public void generateDataSet_R() throws FileNotFoundException{
		Random r = new Random();
		int no_points = 10000;
		PrintWriter writer = new PrintWriter(new File("DataR_small.txt"));
		int left = 1;
		int right = 10000;
		for(int i=0; i<no_points; i+=1){
			
			float x = r.nextFloat() * (10000 - 1) + 1;
			float y = r.nextFloat() * (10000 - 1) + 1;
			
			float h = r.nextFloat() * (20 - 1) + 1;
			float w = r.nextFloat() * (5 - 1) + 1;
			

			if(x + w < right && y + h < right){
				String s = String.format("r%s,%.2f,%.2f,%.2f,%.2f", i, x, y, x+w, y+h);
				writer.println(s);
			}
			
		}
		
		writer.close();
	} 
	
	public static void main(String[] args) throws FileNotFoundException {
		GeneratingDataProb1 p = new GeneratingDataProb1();
		p.generateDataSet_P();
		p.generateDataSet_R();
		p.generateDataSet_P_small();
	}
}
