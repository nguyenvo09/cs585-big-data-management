accesslog= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/accesslog.csv' USING PigStorage(',') as (id:int,byWho:int,whatPage:int,typeAccess:chararray, accessTime:int); 
rec = GROUP accesslog BY (whatPage); 

B = FOREACH rec { 
	GENERATE group, COUNT(accesslog) as cnt;
}
C = ORDER B BY cnt DESC;
D = limit C 10;
--DUMP B;

%declare outfolder 'TaskC_pig.out';
store D into '$outfolder';
