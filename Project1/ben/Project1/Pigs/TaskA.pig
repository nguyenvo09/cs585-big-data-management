mypage= load 'hdfs://localhost:8020/user/hadoop/Facebook/mypage.csv' USING PigStorage(',') as (id,name,country,countryid,hobby); 
A = FILTER mypage BY(country == 'Chinese'); 
B = FOREACH A GENERATE name, hobby; 
-- DUMP B;
/*DUMP B; */
%declare outfolder 'TaskA_pig.out';
store B into '$outfolder';
