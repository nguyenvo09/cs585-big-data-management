mypageTINY= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/mypage.csv' USING PigStorage(',') as (id:int,name:chararray,country:chararray,countryid:int, hobby:chararray); 
rec = GROUP mypageTINY BY (country); 
B = FOREACH rec GENERATE group, COUNT(mypageTINY);
--DUMP B;
%declare outfolder 'TaskB_pig.out';
store B into '$outfolder';