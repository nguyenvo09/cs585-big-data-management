mypage= load 'hdfs://localhost:8020/user/hadoop/Facebook/mypage_TINY.csv' USING PigStorage(',') as (personID:int,personName:chararray,country,countryid,hobby); 
accesslog= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/accesslog_TINY.csv' USING PigStorage(',') as (id:int,byWho:int,whatPage:int,typeAccess:chararray, accessTime:int); 


X = JOIN mypage BY personID RIGHT OUTER, accesslog BY byWho;

Y = FOREACH (GROUP X BY (mypage::personID, personName, whatPage)) GENERATE flatten(group) as (personID, personName, whatPage), COUNT(X) as distinctPages;

Z = FOREACH (GROUP Y BY (personID, personName, distinctPages)) GENERATE flatten(group) as (personID, personName, distinctPages), COUNT(Y) as noAccess;

OUT = FOREACH Z generate personName, distinctPages, noAccess;

dump OUT;

%declare outfolder 'TaskE_pig.out';
store OUT into '$outfolder';


