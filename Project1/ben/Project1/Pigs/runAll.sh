#!/bin/bash

TaskAOut='TaskA_pig.out'
hadoop fs -rmr -r ${TaskAOut}
#pig TaskA.pig
hadoop fs -getmerge ${TaskAOut} ${TaskAOut}


TaskBOut='TaskB_pig.out'
hadoop fs -rmr -r ${TaskBOut}
#pig TaskB.pig
hadoop fs -getmerge ${TaskBOut} ${TaskBOut}


TaskCOut='TaskC_pig.out'
hadoop fs -rmr -r ${TaskCOut}
rm ${TaskCOut}
#pig TaskC.pig
hadoop fs -getmerge ${TaskCOut} ${TaskCOut}


TaskDOut='TaskD_pig.out'
hadoop fs -rmr -r ${TaskDOut}
rm ${TaskDOut}
#pig TaskD.pig
hadoop fs -getmerge ${TaskDOut} ${TaskDOut}



TaskEOut='TaskE_pig.out'
hadoop fs -rmr -r ${TaskEOut}
rm ${TaskEOut}
pig TaskE.pig
hadoop fs -getmerge ${TaskEOut} ${TaskEOut}





