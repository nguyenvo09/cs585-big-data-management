mypage= load 'hdfs://localhost:8020/user/hadoop/Facebook/mypage_TINY.csv' USING PigStorage(',') as (id1,name,country,countryid,hobby); 
friends= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/friends_TINY.csv' USING PigStorage(',') as (id2:int,personID:int,myFriendID:int,dateOfFriend:int, desc:chararray);

X = JOIN mypage BY id1 RIGHT OUTER, friends BY myFriendID;

X = FOREACH X GENERATE mypage::id1 as personID, mypage::name as personName, friends::myFriendID;
OUT = FOREACH (GROUP X BY (personID, personName)) GENERATE flatten(group) as (id, name), COUNT(X) as famous;

OUT = FOREACH OUT generate name, famous;

dump OUT;
%declare outfolder 'TaskD_pig.out';
store OUT into '$outfolder';
