import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import junit.framework.Assert;



public class Testing {

	public static void testTaskD() throws IOException{
		String myPage_tbl = "testfiles/mypage_TINY.csv";
		String friends_tbl = "testfiles/friends_TINY.csv"; 
		BufferedReader reader = new BufferedReader(new FileReader(new File(myPage_tbl)));
		String line = "";
		HashMap<Integer, Integer> mapPerson = new HashMap<Integer, Integer>();
		HashMap<Integer, String> mapName = new HashMap<Integer, String>();
		while((line = reader.readLine()) != null){
			String[] args = line.split(",");
			int personID = Integer.parseInt(args[0]);
			mapPerson.put(personID, 0);
			mapName.put(personID, args[1]);
		}
		reader = new BufferedReader(new FileReader(new File(friends_tbl)));
		line = "";
		while((line = reader.readLine()) != null){
			String[] args = line.split(",");
			int friendID = Integer.parseInt(args[2]);
			Assert.assertTrue(mapPerson.containsKey(friendID));
			
			int vl = mapPerson.get(friendID);
			mapPerson.put(friendID, vl+1);
		}
		for(Entry<Integer, Integer> entry: mapPerson.entrySet()){
			String name = mapName.get(entry.getKey());
			System.out.println(name + " : " + entry.getValue());
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		testTaskD();
	}

}
