
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import junit.framework.Assert;

import org.apache.commons.collections.map.HashedMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.junit.internal.JUnitSystem;

import writable.PairPersonAndHisFriendWritable;
import writable.PairWritable;

/**
 * 
 * @author ben
 *
 */
public class TaskF {

	
	
	private static int FRIEND_TABLE = -1;
	private static int ACCESS_TABLE = -2;

	public static class TokenizerMapper extends
			Mapper<Object, Text, IntWritable, PairWritable> {

		private final static IntWritable one = new IntWritable(1);
		private final static IntWritable two = new IntWritable(2);
		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			if(value.toString().trim().equals("")){
				return;
			}
			String[] s = value.toString().split(",");
			String temp = s[3];
			PairWritable pair = new PairWritable();
			try {
				// Table Friends
				int dateOfFriendShip = Integer.parseInt(temp);
				int personID = Integer.parseInt(s[1]);
				int friendID = Integer.parseInt(s[2]);

				pair.friendID.set(friendID);
				pair.type.set(FRIEND_TABLE);

				context.write(new IntWritable(personID), pair);
			} catch (NumberFormatException e) {
				// Table AccessLog

				int personID = Integer.parseInt(s[1]);
				int friendID = Integer.parseInt(s[2]);
				pair.friendID.set(friendID);
				pair.type.set(ACCESS_TABLE);

				context.write(new IntWritable(personID), pair);

			}
		}
	}

	public static class FindBusyGuysReducer extends
			Reducer<IntWritable, PairWritable, IntWritable, NullWritable> {
		private IntWritable result = new IntWritable(0);
		private static enum PairType {
			JUST_MEET_FRIEND_TABLE, JUST_MEET_ACCESS_LOG, MEET_BOTH_FRIEND_TABLE_AND_ACCESS_LOG
		}
		
		@Override
		protected void setup(
				Reducer<IntWritable, PairWritable, IntWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
		}
		
		public void reduce(IntWritable personID,
				Iterable<PairWritable> access_times_and_friends, Context context)
				throws IOException, InterruptedException {
			
			int x= 0;
			if(personID.get() == 2){
				x += 1;
			}
			HashMap<Integer, PairType> mapFriend = new HashMap<Integer, PairType>();
			try{
				for (PairWritable val : access_times_and_friends) {

					int friendID = val.friendID.get();

					if (val.type.get() == FRIEND_TABLE) {
						if (mapFriend.containsKey(friendID)) {
							//If we already contain friend.
							//There is error in dataset.
							if(Settings.DEBUG){
								PairType p = mapFriend.get(friendID);
								Assert.assertTrue(p == PairType.JUST_MEET_ACCESS_LOG);
							}
							mapFriend.put(friendID, PairType.MEET_BOTH_FRIEND_TABLE_AND_ACCESS_LOG);	
//							throw new Exception("Duplicated entries in Friend Table - Generated data has a problem!!! : " + personID + "," + friendID);
						} else {
							mapFriend.put(friendID, PairType.JUST_MEET_FRIEND_TABLE);	
						}
					} else if (val.type.get() == ACCESS_TABLE) {
						if (mapFriend.containsKey(friendID)) {
							
							PairType p = mapFriend.get(friendID);
							
							if(Settings.DEBUG){
								Assert.assertTrue(p == PairType.JUST_MEET_ACCESS_LOG || p == PairType.JUST_MEET_FRIEND_TABLE || p == PairType.MEET_BOTH_FRIEND_TABLE_AND_ACCESS_LOG);
							}
							
							if(p == PairType.JUST_MEET_ACCESS_LOG){
								mapFriend.put(friendID, PairType.JUST_MEET_ACCESS_LOG);	
							}else{
								//we found both pairs in this case
								mapFriend.put(friendID, PairType.MEET_BOTH_FRIEND_TABLE_AND_ACCESS_LOG);	
							}
							
						} else {
							mapFriend.put(friendID, PairType.JUST_MEET_ACCESS_LOG);		
						}
					}

				}
				boolean isHeNeverVisitedOneOfHisFriends = false;
				for(Entry<Integer, PairType> entry: mapFriend.entrySet()){
					if(entry.getValue() == PairType.JUST_MEET_ACCESS_LOG){
						continue;
					}
					if(entry.getValue() != PairType.MEET_BOTH_FRIEND_TABLE_AND_ACCESS_LOG){
						isHeNeverVisitedOneOfHisFriends = true;
						break;
					}
				}
				if(isHeNeverVisitedOneOfHisFriends){
					context.write(personID, NullWritable.get());
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			

		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 3) {
			System.err.println("Usage: TaskF <HDFS Friends Folder> <HDFS Access Log Folder> <HDFS output folder>");
			System.exit(2);
		}
		String jobname = "TaskF";
		Job job = new Job(conf, jobname);
		job.setJarByClass(TaskF.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setReducerClass(FindBusyGuysReducer.class);
		
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(PairWritable.class);
		
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(PairWritable.class);

		job.setNumReduceTasks(2);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileInputFormat.addInputPath(job, new Path(args[1]));
		FileSystem fs = FileSystem.get(job.getConfiguration());

		String output_folder = args[2];
		fs.delete(new Path(output_folder), true);

		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
