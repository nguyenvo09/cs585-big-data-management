
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import junit.framework.Assert;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import writable.PairPersonAndHisFriendWritable;
import writable.PairWritable;

/**
 * If Task F requires us return only pairs <u> <v> where u does not visit v, then we will 
 * submit this class
 * @author ben
 *
 */
public class TaskF_only_pairs {


	public static class TokenizerMapper extends Mapper<Object, Text, PairPersonAndHisFriendWritable, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private final static IntWritable two = new IntWritable(2);

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			String temp = s[3];
			PairPersonAndHisFriendWritable pair = new PairPersonAndHisFriendWritable();
			try {
				// Table Friends
				int dateOfFriendShip = Integer.parseInt(temp);
				int personID = Integer.parseInt(s[1]);
				int friendID = Integer.parseInt(s[2]);

				pair.friendID.set(friendID);
				pair.personID.set(personID);

				context.write(pair, two);
			} catch (NumberFormatException e) {
				// Table AccessLog

				int personID = Integer.parseInt(s[1]);
				int friendID = Integer.parseInt(s[2]);

				pair.friendID.set(friendID);
				pair.personID.set(personID);

				context.write(pair, one);

			}
		}
	}

	public static class FindBusyGuysReducer extends Reducer<PairPersonAndHisFriendWritable, IntWritable, PairPersonAndHisFriendWritable, NullWritable> {
		
		
		
		public void reduce(PairPersonAndHisFriendWritable pair_personID_friendID,
				Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {
			
			boolean isVisited = false;
			boolean isFriend = false;
			for(IntWritable val : values){
				Assert.assertTrue(val.get() == 1 || val.get() == 2);
				if(val.get() == 2){
					isFriend = true;
				}
				if(val.get() == 1){
					isVisited = true;
				}
			}
//			Assert.assertTrue("They are not friend: " + pair_personID_friendID.toString(), isFriend == true); 
			if(isFriend == true && isVisited == false){
				context.write(pair_personID_friendID, NullWritable.get());
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 3) {
			System.err.println("Usage: TaskF <HDFS Friends Folder> <HDFS Access Log Folder> <HDFS output folder>");
			System.exit(2);
		}
		String jobname = "TaskF";
		Job job = new Job(conf, jobname);
		job.setJarByClass(TaskF_only_pairs.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setReducerClass(FindBusyGuysReducer.class);
		
		job.setMapOutputKeyClass(PairPersonAndHisFriendWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setOutputKeyClass(PairPersonAndHisFriendWritable.class);
		job.setOutputValueClass(NullWritable.class);

		job.setNumReduceTasks(2);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileInputFormat.addInputPath(job, new Path(args[1]));
		FileSystem fs = FileSystem.get(job.getConfiguration());

		String output_folder = args[2];
		fs.delete(new Path(output_folder), true);

		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
