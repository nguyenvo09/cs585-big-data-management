
import java.io.IOException;
import java.util.PriorityQueue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Assert;

/**
 * 
 * @author ben
 *
 */
public class TaskC {

	public static class TokenizerMapper extends
			Mapper<Object, Text, LongWritable, IntWritable> {

		IntWritable vl = new IntWritable();
		LongWritable whatPageKey = new LongWritable();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			String whatPage = s[2];
			whatPageKey.set(Long.parseLong(whatPage));
			vl.set(1);
			context.write(whatPageKey, vl);

		}
	}

	public static class IntSumCombiner extends Reducer<LongWritable, IntWritable, LongWritable, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(LongWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}
	
	/**
	 * Object for minHeap. minHeap will store top10 largest frequency. 
	 * @author hadoop
	 *
	 */
	static class WhatPage implements Comparable<WhatPage>{
		long pageID;
		int count;
		public WhatPage(long _pageID, int _count) {
			// TODO Auto-generated constructor stub
			this.pageID = _pageID;
			this.count = _count;
		}
		@Override
		public int compareTo(WhatPage o) {
			// TODO Auto-generated method stub
			if(o.count > count)
				return -1;
			return 1;
		}
	}
	
	public static class FindTop10Reducer extends Reducer<LongWritable, IntWritable, LongWritable, IntWritable> {
		/**
		 * The maximum size of minHeap.
		 */
		int max_size = 10;
		
		PriorityQueue<WhatPage> minHeap = null;
		LongWritable key_out = new LongWritable();
		IntWritable freq_vl = new IntWritable();

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			minHeap = new PriorityQueue<TaskC.WhatPage>();
		}
		
		public void reduce(LongWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			if(minHeap.size()<max_size){
				minHeap.add(new WhatPage(key.get(), sum));
			}else{
				
				WhatPage top = minHeap.peek();
				//it should be like this. If we found a more popular page, we should add it. 
				if(top.count < sum){
					minHeap.poll();
					//minHeap will know how to adjust the heap.
					minHeap.add(new WhatPage(key.get(), sum));
				}
				
			}
		}
		@Override
		protected void cleanup(
				Reducer<LongWritable, IntWritable, LongWritable, IntWritable>.Context context)
				throws IOException, InterruptedException {
			super.cleanup(context);
			while(!minHeap.isEmpty()){
				WhatPage p = minHeap.poll();
				key_out.set(p.pageID);
				freq_vl.set(p.count);
				context.write(key_out, freq_vl);
			}
		}
	}
	
	

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 2) {
			System.err.println("Usage: TaskC <HDFS input file> <HDFS output file>");
			System.exit(2);
		}
		// set nationality
		String jobname = Settings.TaskC;
		Job job = new Job(conf, jobname);

		job.setJarByClass(TaskC.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.setCombinerClass(IntSumCombiner.class);
		job.setReducerClass(FindTop10Reducer.class);
		job.setNumReduceTasks(1);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileSystem fs = FileSystem.get(job.getConfiguration());

		String output_folder = args[1];
		fs.delete(new Path(output_folder), true);

		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
