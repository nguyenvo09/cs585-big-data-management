package writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

public class PairPersonAndHisFriendWritable implements WritableComparable<PairPersonAndHisFriendWritable>{
	
	public IntWritable personID;
	public IntWritable friendID;
	
	public PairPersonAndHisFriendWritable() {
		// TODO Auto-generated constructor stub
		personID = new IntWritable();
		friendID = new IntWritable();
	}
	
	@Override
	public void readFields(DataInput inp) throws IOException {
		// TODO Auto-generated method stub
		personID.readFields(inp);
		friendID.readFields(inp);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		personID.write(out);
		friendID.write(out);
		
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String s = personID.get() + " " + friendID.get();
		return s;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		String s = toString();
		return s.hashCode();
	}

	@Override
	public int compareTo(PairPersonAndHisFriendWritable o) {
		// TODO Auto-generated method stub
		return this.toString().compareToIgnoreCase(o.toString());
	}
}
