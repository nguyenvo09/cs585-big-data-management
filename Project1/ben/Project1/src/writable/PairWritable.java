package writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

public class PairWritable implements Writable{

	public IntWritable friendID;
	public IntWritable type;
	
	public PairWritable() {
		// TODO Auto-generated constructor stub
		friendID = new IntWritable();
		type = new IntWritable();
	}
	
	@Override
	public void readFields(DataInput inp) throws IOException {
		// TODO Auto-generated method stub
		friendID.readFields(inp);
		type.readFields(inp);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		friendID.write(out);
		type.write(out);
	}
	
	
}
