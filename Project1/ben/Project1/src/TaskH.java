/**
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//package org.apache.hadoop.examples;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskH {

	public static class TokenizerMapper extends Mapper<Object, Text, IntWritable, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private IntWritable k = new IntWritable();

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			int byWho = Integer.parseInt(s[1]);
			k.set(byWho);
			context.write(k, one);
		}
	}
	

	
	public static class IntSumCombiner extends
			Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {
		private IntWritable result = new IntWritable();

		public void reduce(IntWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}
	}
	
	
	public static class IntSumReducer extends Reducer<IntWritable, IntWritable, IntWritable, NullWritable> {
		private IntWritable result = new IntWritable();
		HashMap<Integer, Integer> mapCountFriends = null;
		
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			mapCountFriends = new HashMap<Integer, Integer>();
		}
		
		public void reduce(IntWritable personID, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
			
			int sum = 0;
			for(IntWritable vl: values){
				sum += vl.get();
			}
			mapCountFriends.put(personID.get(), sum);
			
		}
		@Override
		protected void cleanup(Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			int no_users = mapCountFriends.size();
			for(Entry<Integer, Integer> entry: mapCountFriends.entrySet()){
				sum += entry.getValue();
			}
			IntWritable key = new IntWritable();
			double avg = sum / (double) no_users;
//			System.out.println(avg);
			for(Entry<Integer, Integer> entry: mapCountFriends.entrySet()){
				if(entry.getValue() > avg){
					key.set(entry.getKey());
					context.write(key, NullWritable.get());
				}
			}
			// TODO Auto-generated method stub
			super.cleanup(context);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 2) {
			System.err.println("Usage: TaskH <HDFS Friend Table> <HDFS output file>");
			System.exit(2);
		}
		Job job = new Job(conf, "Task H");
		job.setJarByClass(TaskH.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumCombiner.class);
		job.setReducerClass(IntSumReducer.class);
		
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(NullWritable.class);
		
		//Set only one reducer.
		job.setNumReduceTasks(1);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileSystem fs = FileSystem.get(job.getConfiguration());
		
		String output_folder = args[1];
		fs.delete(new Path(output_folder), true);
		
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
