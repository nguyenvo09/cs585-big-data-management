accesslogTINY= LOAD 'hdfs://localhost:8020/user/hadoop/accesslog' USING PigStorage(',') as (accessid:int,visitid:int,pageid:int,type:chararray, accesstime:int); 
A_unique = FOREACH (GROUP accesslogTINY BY (visitid)){
c = accesslogTINY.pageid; 
s = DISTINCT c;
GENERATE group, COUNT(accesslogTINY), COUNT(s);
}; 
DUMP A_unique;

