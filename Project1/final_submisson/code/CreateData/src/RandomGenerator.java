import java.util.Random;

public class RandomGenerator {
	
	public static final String alpha = "abcdefghijklmnopqrstuvwxyz";
	
	public static final String ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static final String[] NATIONALITIES = {"Afghan", "Argentine", "Australian", "Belgian", "Bolivian", "Brazilian", "Cambodian", "Cameroonian", "Canadian", "Chilean", 
			"Chinese", "Colombian", "Costa Rican", "Cuban", "Danish", "Dominican", "Ecuadorian", "Egyptian", "Salvadorian", "English", "Estonian", 
			"Ethiopian", "Finnish", "French", "German", "Ghanaian", "Greek", "Guatemalan", "Haitian", "Honduran", "Indonesian", "Iranian", "Irish", 
			"Israeli", "Italian", "Japanese", "Jordanian", "Kenyan", "Laotian", "Latvian", "Lebanese", "Lithuanian", "Malaysian", "Mexican", "Moroccan", 
			"Dutch", "New Zealander", "Nicaraguan", "Norwegian", "Panamanian", "Paraguayan", "Peruvian", "Filipino", "Polish", "Portuguese", "Puerto Rican", 
			"Romanian", "Russian", "Saudi", "Scottish", "Korean", "Spanish", "Swedish", "Swiss", "Taiwanese", "Tajik", "Thai", "Turkish", "Ukrainian",
			"British", "American", "Uruguayan", "Venezuelan", "Vietnamese", "Welsh", "Other"};
	
	public static final String[] SURNAMES = {
			"Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garcia", "Rodriguez", "Wilson", "Martinez", "Anderson", "Taylor", 
			"Thomas", "Hernandez", "Moore", "Martin", "Jackson", "Thompson", "White", "Lopez", "Lee", "Gonzalez", "Harris", "Clark", "Lewis", "Robinson", 
			"Walker", "Perez", "Hall", "Young", "Allen", "Sanchez", "Wright", "King", "Scott", "Green", "Baker", "Adams", "Nelson", "Hill", "Ramirez", 
			"Campbell", "Mitchell", "Roberts", "Carter", "Phillips", "Evans", "Turner", "Torres", 
			"Parker", "Collins", "Edwards", "Stewart", "Flores", "Morris", "Nguyen", "Murphy", "Rivera", "Cook", "Rogers", "Morgan", "Peterson", "Cooper",
			"Reed", "Bailey", "Bell", "Gomez", "Kelly", "Howard", "Ward", "Cox", "Diaz", "Richardson", "Wood", "Watson", "Brooks", "Bennett", "Gray", "James",
			"Reyes", "Cruz", "Hughes", "Price", "Myers", "Long", "Foster", "Sanders", "Ross", "Morales", "Powell", "Sullivan", "Russell", "Ortiz", "Jenkins", 
			"Gutierrez", "Perry", "Butler", "Barnes", "Fisher"
	};
	
	public static final String[] GIVENNAMES = {
			"Arlene", "Bret", "Cindy", "Don", "Emily", "Franklin", "Gert", "Harvey", "Irma", "Jose", "Katia", "Lee", "Maria", "Nate", "Ophelia", "Philippe", "Rina", "Sean", "Tammy", "Vince", "Whitney", 
			"Alberto", "Beryl", "Chris", "Debby", "Ernesto", "Florence", "Gordon", "Helene", "Isaac", "Joyce", "Kirk", "Leslie", "Michael", "Nadine", "Oscar", "Patty", "Rafael", "Sara", "Tony", "Valerie", "William", 
			"Andrea", "Barry", "Chantal", "Dorian", "Erin", "Fernand", "Gabrielle", "Humberto", "Imelda", "Jerry", "Karen", "Lorenzo", "Melissa", "Nestor", "Olga", "Pablo", "Rebekah", "Sebastien", "Tanya", "Van", "Wendy", 
			"Arthur", "Bertha", "Cristobal", "Dolly", "Eduouard", "Fay", "Gonzalo", "Hanna", "Isaias", "Josephine", "Kyle", "Laura", "Marco", "Nana", "Omar", "Paulette", "Rene", "Sally", "Teddy", "Vicky", "Wilfred", 
			"Ana", "Bill", "Claudette", "Danny", "Elsa", "Fred", "Grace", "Henri", "Ida", "Julian", "Kate", "Larry", "Mindy", "Nicholas", "Odette", "Peter", "Rose", "Sam", "Teresa", "Victor", "Wanda", 
			"Alex", "Bonnie", "Colin", "Danielle", "Earl", "Fiona", "Gaston", "Hermine", "Ian", "Julia", "Karl", "Lisa", "Martin", "Nicole", "Owen", "Paula", "Richard", "Shary", "Tobias", "Virginie", "Walter", ""
	};
	
	// Generates a random number between min and max, inclusive
	public static int getNum(int min, int max, Random random) {
		int x = min;
		x += random.nextInt(max - min + 1);
		return x;
	}
	
	// Generates a random string with length between min and max, inclusive
	public static String getStr(int min, int max) {
		Random random = new Random();
		int length = getNum(min, max, random);
		String s = "";
		for (int i=0; i<length; i++) {
			s += alpha.charAt(random.nextInt(alpha.length()));
		}
		return s;
	}
	
	// Generate a random nationality
	public static String getNat(Random random) {
		return NATIONALITIES[random.nextInt(NATIONALITIES.length)];
	}
	
	// Generate a random name
	public static String getName(Random random) {
		String name = GIVENNAMES[random.nextInt(GIVENNAMES.length)];
		if (random.nextFloat() < 0.3) { name += " " + ALPHA.charAt(random.nextInt(ALPHA.length())); }
		name += " " + SURNAMES[random.nextInt(SURNAMES.length)];
		return name;
	}
}
