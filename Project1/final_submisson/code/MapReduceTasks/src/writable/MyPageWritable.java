package writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class MyPageWritable implements Writable{
	
	private Text myPageName = null;
	private IntWritable friendCount = null;
	public MyPageWritable() {
		myPageName = new Text();
		friendCount = new IntWritable();
	}
	
	@Override
	public void readFields(DataInput inp) throws IOException {
		myPageName.readFields(inp);
		friendCount.readFields(inp);
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		myPageName.write(out);
		friendCount.write(out);
	}
	public void setMyPageName(String myPageName) {
		this.myPageName.set(myPageName);
	}
	public void setFriendCount(int friendCount) {
		this.friendCount.set(friendCount);
	}
	public Text getMyPageName() {
		return myPageName;
	}
	public IntWritable getFriendCount() {
		return friendCount;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return myPageName.toString() + " " + friendCount;
	}

}
