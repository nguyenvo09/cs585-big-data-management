/**
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//package org.apache.hadoop.examples;

import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
/**ben*/
public class TaskE {

	public static class TokenizerMapper extends Mapper<Object, Text, LongWritable, LongWritable> {

		LongWritable ByWhoKey = null;
		LongWritable whatPageValue = null;
		@Override
		protected void setup(
				Mapper<Object, Text, LongWritable, LongWritable>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			ByWhoKey = new LongWritable();
			whatPageValue = new LongWritable();
		}

		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			long ByWho = Long.parseLong(s[1]);
			long whatPage = Long.parseLong(s[2]);
			
			ByWhoKey.set(ByWho);
			whatPageValue.set(whatPage);
			context.write(ByWhoKey, whatPageValue);

		}
	}

	public static class IntSumReducer extends Reducer<LongWritable, LongWritable, LongWritable, Text> {
		private IntWritable result = new IntWritable();
		
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
		}
		public void reduce(LongWritable current_person_key, Iterable<LongWritable> visited_facebook_pages,
				Context context) throws IOException, InterruptedException {
			HashMap<Long, Integer> distinct_visited_facebook_pages = new HashMap<Long, Integer>();
			
			int sum = 0;
			for (LongWritable val : visited_facebook_pages) {
				
				distinct_visited_facebook_pages.put(val.get(), 1);
				//dertermine how many accesses this guy have.
				sum += 1;
			}
			String s = sum + " " + distinct_visited_facebook_pages.size();
			Text t = new Text(s);
			context.write(current_person_key, t);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 2) {
			System.err.println("Usage: TaskE <HDFS Access Log file> <HDFS output file>");
			System.exit(2);
		}
		String jobname = "TaskE";
		Job job = new Job(conf, jobname);
		job.setJarByClass(TaskE.class);
		job.setMapperClass(TokenizerMapper.class);
		//We should not use combiner in this case since we want to know in total how many distinct FB pages, a user visited. 
		job.setReducerClass(IntSumReducer.class);
		
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongWritable.class);
		
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputKeyClass(Text.class);
		
		job.setNumReduceTasks(10);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileSystem fs = FileSystem.get(job.getConfiguration());
		
		String output_folder = args[1];
		fs.delete(new Path(output_folder), true);
		
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
