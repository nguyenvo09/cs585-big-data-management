

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Assert;

/**
 * 
 * @author ben
 *
 */
public class TaskA {

	public static class TokenizerMapper extends Mapper<Object, Text, Text, NullWritable> {

		private Text word = new Text();
		String my_nationality = "";
		@Override
		protected void setup(Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			my_nationality = context.getConfiguration().get("Settings.Nationality");
			
		}
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			Assert.assertTrue(s.length == 5);
			String nationality = s[2];
			String name = s[1];
			String hobby = s[4];
			if(nationality.equalsIgnoreCase(my_nationality)){
				word.set(name + "," + hobby);
				context.write(word, NullWritable.get());
			}
			
		}
	}


	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 3) {
			System.err.println("Usage: wordcount <HDFS input file> <HDFS output file> <nationality>");
			System.exit(2);
		}
		//set nationality
		conf.set("Settings.Nationality", args[2]);
		String jobname="TaskA";
		Job job = new Job(conf, jobname);
		
		job.setJarByClass(TaskA.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(NullWritable.class);
		job.setNumReduceTasks(0);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileSystem fs = FileSystem.get(job.getConfiguration());
		
		String output_folder = args[1];
		fs.delete(new Path(output_folder), true);
		
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
