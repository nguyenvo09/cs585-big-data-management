


import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import writable.MyPageWritable;

/**
 * 
 * @author ben
 *
 */
public class TaskD {

	public static class TokenizerMapper extends Mapper<Object, Text, LongWritable, MyPageWritable> {

		
		LongWritable myPageID_key = null;
		MyPageWritable vl = null;
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			myPageID_key = new LongWritable();
			vl = new MyPageWritable();
			
		}
		
		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			
			try{
				//We should take the second field since this PersonID list MyFriend as a friend
				long myfriend_in_Friends_Table = Long.parseLong(s[2]);
				myPageID_key.set(myfriend_in_Friends_Table);
				vl.setMyPageName("");
				vl.setFriendCount(1);
				context.write(myPageID_key, vl);
			}catch(NumberFormatException e){
				//This indicates that we are reading MyPage table
				long myPageID = Long.parseLong(s[0]);
				String myPageName = s[1];
				myPageID_key.set(myPageID);
				vl.setFriendCount(0);
				vl.setMyPageName(myPageName);
				context.write(myPageID_key, vl);
			}
			

		}
	}

	public static class IntSumCombiner extends Reducer<LongWritable, MyPageWritable, LongWritable, MyPageWritable> {

		public void reduce(LongWritable myPageID, Iterable<MyPageWritable> listFriendCount, Context context) throws IOException, InterruptedException {
			int sum = 0;
			String myPageName = "";
			MyPageWritable outvl = new MyPageWritable();
			
			for (MyPageWritable val : listFriendCount) {
				if(!val.getMyPageName().toString().equalsIgnoreCase("")){
					myPageName = val.getMyPageName().toString();
				}
				sum += val.getFriendCount().get();;
			}
			outvl.setFriendCount(sum);
			outvl.setMyPageName(myPageName);
			context.write(myPageID, outvl);
		}
	}
	
	public static class IntSumReducer extends Reducer<LongWritable, MyPageWritable, MyPageWritable, NullWritable> {

		public void reduce(LongWritable myPageID, Iterable<MyPageWritable> listFriendCount, Context context) throws IOException, InterruptedException {
			int sum = 0;
			String myPageName = "";
			MyPageWritable outvl = new MyPageWritable();
			
			for (MyPageWritable val : listFriendCount) {
				if(!val.getMyPageName().toString().equalsIgnoreCase("")){
					myPageName = val.getMyPageName().toString();
				}
				sum += val.getFriendCount().get();
			}
			outvl.setFriendCount(sum);
			outvl.setMyPageName(myPageName);
			context.write(outvl, NullWritable.get());
		}
	}
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 3) {
			System.err.println("Usage: TaskD <HDFS MyPage Folder> <HDFS Friends Folder> <HDFS_output>");
			System.exit(2);
		}
		String jobname = "TaskD";
		Job job = new Job(conf, jobname);
		job.setJarByClass(TaskD.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(IntSumCombiner.class);
		job.setReducerClass(IntSumReducer.class);
		
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(MyPageWritable.class);
		
		job.setOutputKeyClass(MyPageWritable.class);
		job.setOutputValueClass(NullWritable.class);
		
		job.setNumReduceTasks(2);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileInputFormat.addInputPath(job, new Path(args[1]));

		FileSystem fs = FileSystem.get(job.getConfiguration());
		
		String output_folder = args[2];
		fs.delete(new Path(output_folder), true);
		
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
