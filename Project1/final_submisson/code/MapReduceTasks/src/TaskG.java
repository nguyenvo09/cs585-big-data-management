

import java.io.IOException;
import java.util.PriorityQueue;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.junit.Assert;
/**ben*/
public class TaskG {

	public static class TokenizerMapper extends Mapper<Object, Text, IntWritable, IntWritable> {

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String[] s = value.toString().split(",");
			int byWho = Integer.parseInt(s[1]);
			int accessTime = Integer.parseInt(s[4]);
			IntWritable k = new IntWritable(byWho);
			IntWritable v = new IntWritable(accessTime);			
			context.write(k, v);

		}
	}

	public static class IntSumReducer extends Reducer<IntWritable, IntWritable, IntWritable, NullWritable> {
		
		int initTime = 0;
		@Override
		protected void setup(
				Reducer<IntWritable, IntWritable, IntWritable, NullWritable>.Context context)
				throws IOException, InterruptedException {
			// TODO Auto-generated method stub
			super.setup(context);
			initTime = context.getConfiguration().getInt("Settings.InitThresholdTime", 0);
			
		}
		
		public void reduce(IntWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			
//			PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
			boolean lostInterest = false;
			int min_access_log = -1;
			int max_access_log = -1;
			for(IntWritable vl: values){
				if(min_access_log == -1){
					min_access_log = vl.get();
				}else{
					min_access_log = Math.min(min_access_log, vl.get());
				}
				if(max_access_log == -1){
					max_access_log = vl.get();
					
				}else{
					max_access_log = Math.max(max_access_log, vl.get());
				}
//				if(vl.get() > initTime){
//					//If there is at least a log after that @initTime, this is user is still active.
//					lostInterest = false;
//				}
//				queue.add(vl.get());
			}
			if(min_access_log + initTime > max_access_log){
				//there is no log after that time.
				lostInterest = true;
			}
			
			if(lostInterest){
				context.write(key, NullWritable.get());
			}
			
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		if (args.length != 3) {
			System.err.println("Usage: TaskG <HDFS access log file> <Threshold_Time> <HDFS output file>");
			System.exit(2);
		}
		
		
		conf.setInt("Settings.InitThresholdTime", Integer.parseInt(args[1]));
		
		Job job = new Job(conf, "TaskG");
		job.setJarByClass(TaskG.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setReducerClass(IntSumReducer.class);
		
		
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(NullWritable.class);
		
		job.setNumReduceTasks(5);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileSystem fs = FileSystem.get(job.getConfiguration());
		
		
		
		String output_folder = args[2];
		fs.delete(new Path(output_folder), true);
		
		FileOutputFormat.setOutputPath(job, new Path(output_folder));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
