accesslog= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/accesslog.csv' USING PigStorage(',') as (accessid:int,visitid:int,pageid:int,type:chararray, accesstime:int); 
A_unique = FOREACH (GROUP accesslog BY (visitid)){
c = accesslog.pageid; 
s = DISTINCT c;
GENERATE group, COUNT(accesslog), COUNT(s);}; 

DUMP A_unique;
--%declare outfolder 'LiTaskE_pig.out';
--store A_unique into '$outfolder';
