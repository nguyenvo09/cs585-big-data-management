accesslog= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/accesslog.csv' USING PigStorage(',') as (id:int,byWho:int,whatPage:int,typeAccess:chararray, accessTime:int); 
--10 days x 24 hrs/day x 60 min/hr = 14400
--Assuming the time stamp unit is minutes
X = GROUP accesslog BY byWho;
Y = FOREACH X GENERATE group, MIN(accesslog.accessTime) as firstaccess, MAX(accesslog.accessTime) as lastaccess;
Z = FILTER Y BY (lastaccess < firstaccess + 14400 AND firstaccess + 14400 < 1000000);
--We also want to make sure the page wasn't made too recently before marking it as "lost interest"
result = FOREACH Z GENERATE $0;
dump result;
--%declare outfolder 'TaskG_pig.out';
--store result into '$outfolder';
