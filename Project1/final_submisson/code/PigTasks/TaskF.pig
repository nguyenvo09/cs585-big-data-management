accesslog= LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/accesslog.csv' USING PigStorage(',') as (accessid:int,visitid:int,pageid:int,type:chararray, accesstime:int); 
friends = LOAD 'hdfs://localhost:8020/user/hadoop/Facebook/friends.csv' USING PigStorage(',') as (rel:int,id:int,friendid:int,date:chararray, Desc:chararray); 
outer_left = JOIN friends by (id,friendid) LEFT OUTER, accesslog by (visitid, pageid); 
mark = FOREACH outer_left GENERATE $1, $2, $7; 
X = FILTER mark by $2 is NULL; 
result = foreach X generate $0, $1;
DUMP result;
--%declare outfolder 'LiTaskF_pig.out';
--store result into '$outfolder';
