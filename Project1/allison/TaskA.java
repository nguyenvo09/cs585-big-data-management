package project1group12;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskA {
	
  public static String my_nationality = "Chinese";

  public static class TaskAMapper
       extends Mapper<Object, Text, Text, Text>{

    private Text word = new Text();

    @Override
    protected void setup(Context context)
      throws IOException, InterruptedException {
      // TODO Auto-generated method stub
      super.setup(context);
      my_nationality = context.getConfiguration().get("Settings.Nationality");
    }
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      if (result[2].equals(my_nationality)) {
        word.set(result[1] + "," + result[4]);
        context.write(word, null);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    if (args.length < 2) {
      System.err.println("Usage: taska <HDFS input file> <HDFS output file> <nationality>");
      System.exit(2);
    }
    //set nationality
    conf.set("Settings.Nationality", args[2]);
    Job job = new Job(conf, "task a");
    job.setJarByClass(TaskA.class);
    job.setMapperClass(TaskAMapper.class);
    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(0); // map-only job
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
