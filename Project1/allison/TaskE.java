package project1group12;

import java.io.IOException;
import java.util.HashSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskE {

  public static class TaskEMapper
       extends Mapper<Object, Text, Text, Text>{
    
    private Text id = new Text();
    private Text accessed = new Text();
      
    // <person who accessed, page accessed>
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      id.set(result[1]); // ID of person who accessed
      accessed.set(result[2]);
      context.write(id, accessed);
      }
    }
  
  public static class TaskEReducer 
       extends Reducer<Text,Text,Text,Text> {
    private Text result = new Text();

    public void reduce(Text key, Iterable<Text> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int total = 0;
      HashSet<String> distinct = new HashSet<String>();
      for (Text val : values) {
        distinct.add(val.toString());
        total += 1;
      }
      result.set(total + "," + distinct.size());
      context.write(key, result);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    if (args.length != 2) {
      System.err.println("Usage: taske <HDFS input file> <HDFS output file>");
      System.exit(2);
    }
    Job job = new Job(conf, "task e");
    job.setJarByClass(TaskE.class);
    job.setMapperClass(TaskEMapper.class);
    //job.setCombinerClass(TaskEReducer.class);
    job.setReducerClass(TaskEReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(2);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
