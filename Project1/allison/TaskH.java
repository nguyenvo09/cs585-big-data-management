import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskH {
  
  public static int total;

  // calculates total number of pages
  // we do not want to assume there will always be 100000 pages
  // input: mypage
  public static class TaskH1Mapper
       extends Mapper<Object, Text, Text, IntWritable>{
    
    private int total;

    public void setup(Context context) throws IOException, InterruptedException {
      total = 0;
    }
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      total++;
      //context.getCounter("total","total").increment(1);
    }

    public void cleanup(Context context) throws IOException, InterruptedException {
      context.write(new Text("-999"), new IntWritable(total)); 
    }
  }

  // input: friends; <page, 1>
  // this is similar to TaskD2Mapper
  // sharing opportunities??? It seems that Tasks D and H can be computed simultaneously.
  public static class TaskH2Mapper
       extends Mapper<Object, Text, Text, IntWritable>{
    
    private Text id = new Text(); 
    private final IntWritable one = new IntWritable(1);

    private int total;

    public void setup(Context context) throws IOException, InterruptedException {
      total = 0;
    }
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      //context.getCounter("total sum","total sum").increment(1);
      total++;
      String[] result = value.toString().split(",");
      id.set(result[2]); // ID of person who was friended
      context.write(id, one);
    }

    public void cleanup(Context context) throws IOException, InterruptedException {
      context.write(new Text("-888"), new IntWritable(total)); 
    }
  }

  public static class TaskHCombiner
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }
  
  public static class TaskHReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    private double total_pages = 0.0;
    private double total_friendships = 0.0;

    public void reduce(Text key, Iterable<IntWritable> values, 
                       Context context
                       ) throws IOException, InterruptedException {

      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }

      if (key.toString().equals("-999")) { total_pages = (double) sum; }
      else if (key.toString().equals("-888")) { total_friendships = (double) sum; }
      else {
        result.set(sum);
        if (sum > total_friendships / total_pages) {
          context.write(key, null);
        }
      }
    }
  }

  /*public static class FilterMapper
       extends Mapper<Text, Text, Text, Text>{

    private float avg;

    @Override
    protected void setup(Context context)
      throws IOException, InterruptedException {
      super.setup(context);
      avg = context.getConfiguration().getFloat("Settings.Average",0);
    }
      
    public void map(Text key, Text value, Context context
                    ) throws IOException, InterruptedException {
      if (Float.parseFloat(value.toString()) > avg) {
        context.write(key, null);
      }
    }
  }*/

  public static void main(String[] args) throws Exception {
    //String inter_path = "p1h_inter";
    Configuration conf = new Configuration();
    if (args.length != 3) {
      System.err.println("Usage: taskh <HDFS input file> <HDFS input file> <HDFS output file>");
      System.exit(2);
    }
    Job job = new Job(conf, "task h");
    job.setJarByClass(TaskH.class);

    MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, TaskH1Mapper.class);
    MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, TaskH2Mapper.class);
    //job.setCombinerClass(TaskHReducer.class);
    job.setCombinerClass(TaskHCombiner.class);
    job.setReducerClass(TaskHReducer.class);

    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(1);  // single reducer
    job.setOutputValueClass(IntWritable.class);
    FileOutputFormat.setOutputPath(job, new Path(args[2]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);

/*
    FileOutputFormat.setOutputPath(job, new Path(inter_path));
    job.waitForCompletion(true);

    float avg = (float) job.getCounters().findCounter("total sum","total sum").getValue() / (float) job.getCounters().findCounter("total","total").getValue();

    System.out.println("Average: " + avg);

    // Second job filters only pages friended greater than average
    conf.setFloat("Settings.Average", avg);
    Job job2 = new Job(conf, "task h2");
    job2.setJarByClass(TaskH.class);
    job2.setMapperClass(FilterMapper.class);
    job2.setOutputKeyClass(Text.class);
    job2.setInputFormatClass(KeyValueTextInputFormat.class);
    job2.setNumReduceTasks(0); // map-only job
    job2.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job2, new Path(inter_path));
    FileOutputFormat.setOutputPath(job2, new Path(args[2]));

    System.exit(job2.waitForCompletion(true) ? 0 : 1);*/
  }
}
