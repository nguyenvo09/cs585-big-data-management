package project1group12;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskD {

  // input: mypage; <ID, name>
  public static class TaskD1Mapper
       extends Mapper<Object, Text, Text, Text>{
    
    private Text id = new Text(); 
    private Text name = new Text();
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      id.set(result[0]);
      name.set("name," + result[1]);
      context.write(id, name);
    }
  }

  // input: friends; <ID, 1>
  public static class TaskD2Mapper
       extends Mapper<Object, Text, Text, Text>{
    
    private Text id = new Text(); 
    private Text one = new Text("num,1");
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      id.set(result[2]); // ID of person who was friended
      context.write(id, one);

    }
  }
  
  // join and aggregate
  public static class TaskDReducer 
       extends Reducer<Text,Text,Text,IntWritable> {
    private IntWritable result = new IntWritable();
    private Text name = new Text();

    public void reduce(Text key, Iterable<Text> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (Text val : values) {
        String[] s = val.toString().split(",");
        if (s[0].equals("name")) {
          name.set(s[1]);
        } else {
          sum += 1;
        }
      }
      result.set(sum);
      context.write(name, result);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    if (args.length != 3) {
      System.err.println("Usage: taskd <HDFS input file> <HDFS input file> <HDFS output file>");
      System.exit(2);
    }
    Job job = new Job(conf, "task d");
    job.setJarByClass(TaskD.class);
    //job.setMapperClass(TaskDMapper.class);
    MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, TaskD1Mapper.class);
    MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, TaskD2Mapper.class);
    //job.setCombinerClass(TaskDCombiner.class);
    job.setReducerClass(TaskDReducer.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(2);
    job.setOutputValueClass(IntWritable.class);
    FileOutputFormat.setOutputPath(job, new Path(args[2]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
