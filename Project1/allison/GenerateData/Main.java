import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

	public static void main(String[] args) {
		String mypage = "";
		String friends = "";
		String accesslog = "";
		
		int numUsers = 100000;
		int numRel = 5000;
		int numAccess = 10000;
		
		for (int i=0; i<args.length; i++) {
			if (args[i].equals("-mypage")) mypage = args[++i];
			if (args[i].equals("-friends")) friends = args[++i];
			if (args[i].equals("-accesslog")) accesslog = args[++i];
			
			if (args[i].equals("-u")) numUsers = Integer.parseInt(args[++i]);
			if (args[i].equals("-r")) numRel = Integer.parseInt(args[++i]);
			if (args[i].equals("-a")) numAccess = Integer.parseInt(args[++i]);
		}
		
		Random random = new Random();
		
		// MyPage: ID, Name, Nationality, CountryCode, Hobby
		try {			 
			File output_file = new File(mypage);
			BufferedWriter output = new BufferedWriter(new FileWriter(output_file));
			
			for (int i=0; i<numUsers; i++) {
				output.append(i+1 + "," + RandomGenerator.getName(random) + "," + RandomGenerator.getNat(random) + "," + 
						RandomGenerator.getNum(1, 10, random) + "," + RandomGenerator.getStr(10, 20) + "\n");
			}
			output.close();
		} catch (IOException e) { e.printStackTrace(); }
		
		// Friends: FriendRel, PersonID, MyFriend, DateOfFriendship, Desc
		try {			 
			File output_file = new File(friends);
			BufferedWriter output = new BufferedWriter(new FileWriter(output_file));
			HashMap<Integer, HashSet<Integer>> friended = new HashMap<Integer, HashSet<Integer>>();
			
			for (int i=0; i<numRel; i++) {
				int user = RandomGenerator.getNum(1, numUsers, random);
				if (!friended.containsKey(user)) { friended.put(user, new HashSet<Integer>()); }
				int other = user;
				// A person cannot friend himself, and he cannot friend the same person twice (?)
				while (other == user) { 
					int temp = RandomGenerator.getNum(1,  numUsers, random);
					if (!friended.get(user).contains(temp)) { 
						other = temp;
						friended.get(user).add(other);
					}
				}
				
				output.append(i+1 + "," + user + "," + other + "," + RandomGenerator.getNum(1, 1000000, random) + "," + RandomGenerator.getStr(20, 50) + "\n");
			}
			output.close();
		} catch (IOException e) { e.printStackTrace(); }
		
		// AccessLog: AccessId, ByWho, WhatPage, TypeOfAccess, AccessTime
		try {			 
			File output_file = new File(accesslog);
			BufferedWriter output = new BufferedWriter(new FileWriter(output_file));
			
			for (int i=0; i<numAccess; i++) {
				int user = RandomGenerator.getNum(1, numUsers, random);
				int other = user;
				// We ignore times when the user accesses his own page
				while (other == user) { 
					other = RandomGenerator.getNum(1,  numUsers, random); 
				}
				
				output.append(i+1 + "," + user + "," + other + "," + RandomGenerator.getStr(20, 50) + "," + RandomGenerator.getNum(1, 1000000, random) + "\n");
			}
			output.close();
		} catch (IOException e) { e.printStackTrace(); }

	}

}
