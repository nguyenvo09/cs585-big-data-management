package project1group12;

import java.io.IOException;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskC {

  public static int TOPNUM = 10; // 10
  public static int count = 0;

  public static class TaskCMapper
       extends Mapper<Object, Text, Text, IntWritable>{
    
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      word.set(result[2]); // ID of page that was accessed
      context.write(word, one);
      }
    }

  public static class TaskCCombiner 
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();
 
    public void reduce(Text key, Iterable<IntWritable> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }
  
  // in this case, I only return 10 pages even if they all have the same access counts
  // another solution is to return the top 10 access counts with possibly multiple pages per count
  public static class TaskCReducer 
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private Text word = new Text(); 
    private IntWritable num = new IntWritable();
    private TreeMap<Integer, ArrayList<String>> top = new TreeMap<Integer, ArrayList<String>>(); // to store top values

    // aggregate access counts and fill tree up to 10 pages
    public void reduce(Text key, Iterable<IntWritable> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      Integer ksum = new Integer(sum);
      if (count < TOPNUM) {
        if (top.keySet().contains(ksum)) {
          top.get(ksum).add(key.toString());
        } else {
          top.put(ksum, new ArrayList<String>(Arrays.asList(key.toString())));
        }
        count++;
      } else {
        Integer min = top.firstKey(); // smallest access count in tree
        if (ksum > min) {
          // add page to top tree
          if (top.keySet().contains(ksum)) {
            top.get(ksum).add(key.toString());
          } else {
            top.put(ksum, new ArrayList<String>(Arrays.asList(key.toString())));
          }
          // delete page with min access counts in top tree
          if (top.get(min).size() == 1) {
            top.remove(min);
          } else {
            top.get(min).remove(top.get(min).size()-1);
          }
        }
      }
    }

    // iterate through tree and write out all pages
    public void cleanup(Context context) throws IOException, InterruptedException {
      for (Integer j : top.descendingKeySet()) {
        num.set(j);
        for (String s : top.get(j)) {
          word.set(s);
          context.write(word, num);
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    if (args.length != 2) {
      System.err.println("Usage: taskc <HDFS input file> <HDFS output file>");
      System.exit(2);
    }

    // First job calculates how many times a person's page was accessed
    Job job = new Job(conf, "task c");
    job.setJarByClass(TaskC.class);
    job.setMapperClass(TaskCMapper.class);
    job.setCombinerClass(TaskCCombiner.class);
    job.setReducerClass(TaskCReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(1);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
