accesslogTINY= LOAD 'hdfs://localhost:8020/user/hadoop/accesslog_TINY' USING PigStorage(',') as (accessid:int,visitid:int,pageid:int,type:chararray, accesstime:int); 
friendsTINY = LOAD 'hdfs://localhost:8020/user/hadoop/friends_TINY' USING PigStorage(',') as (rel:int,id:int,friendid:int,date:chararray, Desc:chararray); 
outer_left = JOIN friendsTINY by (id,friendid) LEFT OUTER, accesslogTINY by (visitid, pageid); 
mark = FOREACH outer_left GENERATE $1, $2, $7; 
X = FILTER mark by $2 is NULL; 
result = foreach X generate $0, $1;
DUMP result;
--%declare outfolder 'LiTaskF_pig.out';
--store result into '$outfolder';
