package project1group12;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class TaskF {

  // input: accesslog; <(ID,page), "access">
  public static class TaskF1Mapper
       extends Mapper<Object, Text, Text, Text>{
    
    private Text text_tuple = new Text(); 
    private Text label = new Text("access");
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      text_tuple.set(result[1] + "," + result[2]); // ID,page
      context.write(text_tuple, label);
    }
  }

  // input: friends; <(ID,friend), "friend">
  public static class TaskF2Mapper
       extends Mapper<Object, Text, Text, Text>{
    
    private Text text_tuple = new Text(); 
    private Text label = new Text("friend");
      
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String[] result = value.toString().split(",");
      text_tuple.set(result[1] + "," + result[2]); // ID,friend
      context.write(text_tuple, label);

    }
  }

  // join; <(ID, page), description>
  // eliminate duplicates for "access"
  // Note: we assume each friend declaration is unique
  public static class TaskFCombiner 
       extends Reducer<Text,Text,Text,Text> {

    private Text description = new Text();

    public void reduce(Text key, Iterable<Text> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      int count_access = 0;
      String s = "";
      for (Text val : values) {
        if (val.toString().equals("friend")) {
          context.write(key, val);
        } else if (count_access == 0) {
          context.write(key, val);
          count_access++;
        }
      }
    }
  }
  
  // join; <ID, friend>
  public static class TaskFReducer 
       extends Reducer<Text,Text,Text,Text> {
    private Text id = new Text();
    private Text friend = new Text();

    public void reduce(Text key, Iterable<Text> values, 
                       Context context
                       ) throws IOException, InterruptedException {
      boolean write = true;
      for (Text val : values) {
        if (val.toString().equals("access")) {
          write = false;
          break;
        }
      }
      if (write) {
        String[] line = key.toString().split(",");
        id.set(line[0]);
        friend.set(line[1]);
        context.write(id, friend);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    if (args.length != 3) {
      System.err.println("Usage: taskf <HDFS input file> <HDFS input file> <HDFS output file>");
      System.exit(2);
    }
    Job job = new Job(conf, "task f");
    job.setJarByClass(TaskF.class);
    //job.setMapperClass(TaskDMapper.class);
    MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, TaskF1Mapper.class);
    MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, TaskF2Mapper.class);
    job.setCombinerClass(TaskFCombiner.class);
    job.setReducerClass(TaskFReducer.class);
    //job.setMapOutputKeyClass(Text.class);
    //job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(Text.class);
    job.setNumReduceTasks(2);
    job.setOutputValueClass(Text.class);
    FileOutputFormat.setOutputPath(job, new Path(args[2]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
