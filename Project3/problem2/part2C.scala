/**
  * Created by ben
  */
import org.apache.spark.{SparkConf, SparkContext}
import java.io.PrintWriter
import java.io.File
import org.apache.hadoop.fs._
import org.apache.hadoop.conf.Configuration
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.PriorityQueue



object part2C {

  val side_constant = 20
  val K_constant = 100
  val N = 500
  val DEBUG = false
  def processEachPoint(line : String): (String, Int) ={
    val p = line.split(",")
    val x = p(0).toInt - 1
    val y = p(1).toInt - 1
    //find single cell id
    val side = side_constant
    val r = y/side
    val c = x/side
    //ID of cell
    val cellID= r+","+c
    return (cellID, 1)
  }

  /**
    * For each singleCell, we need to find all super cells that it belongs to
    * We want to find supercells it belongs to because we need to compute average
    * number of points of neighbers of center of a supercell.
    *
    * @param singleCell
    * @return
    */
  def mapCellIS(singleCell: (String, Double)) : List[(String, Double)] = {
    val cellID = singleCell._1.split(",")
    val row = cellID(0).toInt
    val col = cellID(1).toInt
    val noPointsInThatCell = singleCell._2
    //find all neighbors of this singleCell
    val dr = Array(0, 0, 1, -1, 1, -1,  1, -1)
    val dc = Array(-1, 1, 0, 0, 1, -1, -1,  1)
//    val N = 20000
    val res = new ListBuffer[(String, Double)]()
    //res += ((singleCell._1, noPointsInThatCell))  //center point
    for(i <- 0 to 7){
      val newR = dr(i) + row
      val newC = dc(i) + col
      if(newR >= 0 && newR < N && newC >= 0 && newC < N){
        val superCellID = newR+","+newC
        res += ((superCellID, noPointsInThatCell))
      }

    }
    return res.toList
    //    return null;
  }
  //sort and trim a traversable (String, Long) tuple by _2 value of the tuple
  def topNs(xs: TraversableOnce[Double], n: Int) : List[Double] = {
    val x = PriorityQueue.empty[Double] //min heap
    System.out.println("Here!!!!");
    var len = 0
    xs foreach { e =>
      if(x.size < K_constant){
        x.enqueue(e)
      }else{
        //try{
        val top = x.head

        if(top < e){
          x.dequeue()
          x.enqueue(e)
        }
        //        }catch{
        //          case e:Exception => System
        //        }
      }

    }
    var ls = new ListBuffer[Double]()
    while(!x.isEmpty){
      ls += x.dequeue()
    }
    return ls.toList
  }
  def createCombiner(value: String): Seq[String] = Seq(value);

  //add the incoming value to the accumulating top N list for the key
  def mergeValue(combined: Seq[String], value:  String): Seq[String] = combined ++ Seq(value)

  //merge top N lists returned from each partition into a new combined top N list
  def mergeCombiners(combined1: Seq[String], combined2: Seq[String]): Seq[String] = {
    val vl = combined1 ++ combined2
    var numerator = 0.0
    var denominator = 0.0
    vl foreach{
      e =>
        val parts = e.split(",")
        if(parts(1).equalsIgnoreCase("denominator")){
          denominator = parts(0).toDouble
        }else if (parts(1).equalsIgnoreCase("numerator")){
          numerator = parts(0).toDouble
        }
    }
    val x = numerator / denominator + ""
    return Seq(x)
  }

  def mapSuperCell(superCell: (String, Double)) : (String, String) = {
    val id_ = superCell._1.split(",")
    val r = id_(0).toInt
    val c = id_(1).toInt

    val dr = Array(0, 0, 1, -1, 1, -1,  1, -1)
    val dc = Array(-1, 1, 0, 0, 1, -1, -1,  1)
//    val N = 20000
    //res += ((singleCell._1, noPointsInThatCell))  //center point
    var cnt = 0
    for(i <- 0 to 7){
      val newR = dr(i) + r
      val newC = dc(i) + c
      if(newR >= 0 && newR < N && newC >= 0 && newC < N){
        cnt += 1
      }

    }
    return (superCell._1, superCell._2 / cnt.toDouble + "," + "denominator")
  }
  def mapAfterComputeIS(x: Seq[String]) : Double = {
    val e = x.head
    val parts = e.split(",")
    if(parts.size == 1){
      return e.toDouble
    }
    return 0.0
  }
  def reportISNeighborOfTop(superCell: (String, Double), topSupercells: Array[(String, Double)]) : List[(String, Double)] = {
    val id_ = superCell._1.split(",")
    val r = id_(0).toInt
    val c = id_(1).toInt
    val dr = Array(0, 0, 1, -1, 1, -1,  1, -1)
    val dc = Array(-1, 1, 0, 0, 1, -1, -1,  1)
//    val N = 20000
    val res = new ListBuffer[(String, Double)]()
    for(i <- 0 to 7){
      val newR = dr(i) + r
      val newC = dc(i) + c
      if(newR >= 0 && newR < N && newC >= 0 && newC < N){
        //valid neighbor.
        val new_neighbr = newR + "," + newC;
        for(super_ <- topSupercells){
          val topID = super_._1
          val irs = super_._2
          if(new_neighbr.equalsIgnoreCase(topID)){
            res += ((topID, superCell._2))
          }
        }
      }

    }
    return res.toList
  }

  def createCombiner2(value: Double): String = {
    if(math.abs(value) < 1e-5){
      return "";
    }else {
      return value + ""
    }
  };

  //add the incoming value to the accumulating top N list for the key
  def mergeValue2(combined: String, value:  Double):String = {
    if(math.abs(value) < 1e-5){
      return combined
    }else {
      return combined + "," + value
    }
  }

  //merge top N lists returned from each partition into a new combined top N list
  def mergeCombiners2(combined1: String, combined2: String): String = {
    combined1+","+combined2
  }
  def convertRowColToID(cellIDPair: String) : String = {
    val p = cellIDPair.split(",")
    val r = p(0).toInt //for x
    val c = p(1).toInt  //for y
    val _id_ = (499 - r)*500 + c + 1
    return "cell_" + _id_
  }
  def convertToCellID(ss: (String, String)) : (String, String) = {
    val key = ss._1
    val value = ss._2

    val s = convertRowColToID(key)
    return (s, value)
  }
  def main(args: Array[String]) {

    //Create a SparkContext to initialize Spark
    val conf = new SparkConf()
    conf.setMaster("local")
    conf.setAppName("Part2B")
    val sc = new SparkContext(conf)

    // Load the text into a Spark RDD, which is a distributed representation of each line of text
    val textFile = sc.textFile(args(0))

    //word count
    val countPointEachCellRDD = textFile.map(processEachPoint).reduceByKey((x, y) => (x+y)).mapValues(z => z*1.0)


    val hadoopConf = new Configuration()
    val hdfs = FileSystem.get(hadoopConf)
    val local_file = FileSystem.getLocal(hadoopConf)

    hdfs.delete(new Path(args(1)), true)

    if(DEBUG){
      hdfs.delete(new Path("countPointInEachCell"), true)
      countPointEachCellRDD.saveAsTextFile("countPointInEachCell")
    }

    val denominatorEachSuperCellRDD = countPointEachCellRDD
      .flatMap(mapCellIS)
      .reduceByKey((x,y) => (x+y)) //compute total points of neighbors around.
      .map(mapSuperCell) //compute average value of neighbors

    if(DEBUG){
      hdfs.delete(new Path("denominator"), true)
      denominatorEachSuperCellRDD.saveAsTextFile("denominator")
    }
    val numerator = countPointEachCellRDD.mapValues(z => z + "," + "numerator")
    //    val allData = numerator.union(denominatorEachSuperCellRDD).reduceByKey((x, y) => x/(y+1))
    val allData = numerator.union(denominatorEachSuperCellRDD).combineByKey(
      createCombiner,
      mergeValue,
      mergeCombiners)
      .mapValues(mapAfterComputeIS)

    /**
      * Note that this array is stored on Master node.
      */
    val vl: Array[(String, Double)] = allData.takeOrdered(K_constant)(Ordering[Double].reverse.on { x => x._2 })
    //    val vl = Array((1,2))

    val res = allData.flatMap(x => reportISNeighborOfTop(x, vl)).combineByKey(createCombiner2, mergeValue2, mergeCombiners2).map(convertToCellID)
    res.saveAsTextFile(args(1))
  }
}
