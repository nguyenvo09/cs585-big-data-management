# README #

### What is this repository for? ###

* This is respository containing all projects in CS585 - Big Data Management at WPI - instructed by Professor Elke Rundensteiner 
* [Course website](http://web.cs.wpi.edu/~cs585/f17/)

### Team members ###

* Rozet, Allison Mieko - amrozet@wpi.edu
* Vo, Nguyen - nkvo@wpi.edu
* Li, Shanhao - sli7@wpi.edu
